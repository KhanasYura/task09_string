package com.khanas;

public class StringUtils {

    public final String getString(final Object... objects) {
        StringBuilder sb = new StringBuilder();
        for (Object object : objects) {
            sb.append(object).append(" ");
        }
        return sb.toString();
    }
}
