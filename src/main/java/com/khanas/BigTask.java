package com.khanas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BigTask {

    private String text;
    private ArrayList<String> sentences;
    private ArrayList<String> words;
    private ArrayList<String> punctuation;
    private static Logger logger = LogManager.getLogger();
    private Scanner sc = new Scanner(System.in);

    public BigTask() {

        readFromFile();
        System.out.println(text);
        task();
        logger.info("------------Task1------------");
        task1();
        logger.info("------------Task2------------");
        task2();
        task();
        logger.info("------------Task3------------");
        task3();
        logger.info("------------Task4------------");
        task4();
        logger.info("------------Task5------------");
        task5();
        logger.info("------------Task6------------");
        task6();
        logger.info("------------Task7------------");
        task7();
        logger.info("------------Task8------------");
        task8();
        logger.info("------------Task9------------");
        task9();
        logger.info("------------Task10------------");
        task10();
        logger.info("------------Task11------------");
        task11();
        task();
        logger.info("------------Task12------------");
        task12();
        readFromFile();
        logger.info("------------Task13------------");
        task13();
        logger.info("------------Task14------------");
        task14();
        logger.info("------------Task15------------");
        task15();
        readFromFile();
        logger.info("------------Task16------------");
        task16();
    }

    private void readFromFile() {
        try {
            StringBuilder sb = new StringBuilder();
            String sCurrentLine;
            BufferedReader bufferedReader = new BufferedReader(
                    new FileReader("text.txt"));

            while ((sCurrentLine = bufferedReader.readLine()) != null) {
                sb.append(sCurrentLine);
            }

            text = sb.toString().replaceAll("\\s+", " ");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void task() {

        sentences = getSentences(text);
        System.out.println(sentences);

        words = getWords(text);
        System.out.println(words);

        Pattern patternPunctuations = Pattern.compile(
                "[.,\\/#!$%\\^&\\*;:{}=\\-_`~()]");
        Matcher matcherPunctuations = patternPunctuations.matcher(text);
        punctuation = new ArrayList<>();

        while (matcherPunctuations.find()) {
            punctuation.add(text.substring(matcherPunctuations.start(),
                    matcherPunctuations.end()));
        }

        System.out.println(punctuation);

    }

    private void task1() {
        int[] infoAboutWords = new int[words.size()];
        Arrays.fill(infoAboutWords, 0);

        for (String sentence : sentences) {
            for (int i = 0; i < words.size(); i++) {
                if (sentence.contains(words.get(i))) {
                    infoAboutWords[i]++;
                }
            }
        }

        logger.info("Max value" + Arrays.stream(infoAboutWords).max().getAsInt());
    }


    private ArrayList<String> getWords(String string) {
        Pattern patternWords = Pattern.compile("[A-Za-z]+");
        Matcher matcherWords = patternWords.matcher(string);
        ArrayList<String> words = new ArrayList<>();

        while (matcherWords.find()) {
            words.add(string.substring(matcherWords.start(), matcherWords.end()));
        }

        return words;
    }

    private ArrayList<String> getSentences(String string) {
        Pattern patternSentence = Pattern.compile("[A-Z][\\w',]*( [\\w',]+)*[!?.]");
        Matcher matcherSentence = patternSentence.matcher(string);
        ArrayList<String> sentence = new ArrayList<>();

        while (matcherSentence.find()) {
            sentence.add(text.substring(matcherSentence.start(), matcherSentence.end()));
        }

        return sentence;
    }

    private int consonant(String word) {
        String consonant = "BCDFGHJKLMNPQRSTVWXZ";

        for (int index = 0; index < word.length(); index++) {
            if (consonant.contains(String.valueOf(word.toUpperCase().charAt(index)))) {
                return index;
            }
        }

        return 1000;
    }

    private void task2() {
        logger.info("Before:");
        logger.info(sentences);

        Integer[] arr = new Integer[sentences.size()];

        for (int i = 0; i < sentences.size(); i++) {
            Pattern patternWords = Pattern.compile("[A-Za-z]+");
            Matcher matcherWords = patternWords.matcher(sentences.get(i));
            ArrayList<String> word = new ArrayList<>();
            int count = 0;

            while (matcherWords.find()) {
                word.add(sentences.get(i).substring(matcherWords.start(),
                        matcherWords.end()));
                count++;
            }

            arr[i] = count;
        }

        for (int i = 0; i < sentences.size(); i++) {
            for (int j = 0; j < sentences.size(); j++) {
                if (arr[i] > arr[j]) {
                    String s = sentences.get(i);
                    sentences.set(i, sentences.get(j));
                    sentences.set(j, s);
                }
            }
        }

        logger.info("After:");
        logger.info(sentences);
    }

    private void task3() {
        Pattern patternWords = Pattern.compile("[A-Za-z]+");
        Matcher matcherWords = patternWords.matcher(sentences.get(0));
        ArrayList<String> word = new ArrayList<>();

        while (matcherWords.find()) {
            word.add(sentences.get(0).substring(matcherWords.start(),
                    matcherWords.end()));
        }

        for (String s : word) {
            int count = 0;

            for (int i = 1; i < sentences.size(); i++) {
                if (sentences.get(i).indexOf(s) != -1) {
                    count++;
                    break;
                }
            }

            if (count == 0) {
                logger.info(s);
            }
        }

    }

    private void task4() {
        Set<String> word = new HashSet<>();
        Pattern patternSentence = Pattern.compile("[A-Z][\\w',]*( [\\w',]+)*[?]");
        Matcher matcher = patternSentence.matcher(text);
        ArrayList<String> sentence = new ArrayList<>();

        while (matcher.find()) {
            sentence.add(text.substring(matcher.start(), matcher.end()));
        }

        logger.info("Enter length of the string:");
        int length = sc.nextInt();

        for (int i = 0; i < sentence.size(); i++) {
            Pattern patternWords = Pattern.compile("[A-Za-z]+");
            Matcher matcherWords = patternWords.matcher(sentence.get(i));

            while (matcherWords.find()) {
                if (sentence.get(i).substring(matcherWords.start(),
                        matcherWords.end()).length() == length) {
                    word.add(sentence.get(i).substring(matcherWords.start(),
                            matcherWords.end()));
                }
            }
        }

        System.out.println(word);
    }

    private void task5() {
        ArrayList<String> sentence = getSentences(text);

        for (int i = 0; i < sentence.size(); i++) {
            ArrayList<String> word = getWords(sentence.get(i));
            Pattern patternWord = Pattern.compile("[AEIOUY]+");
            Matcher matcherword = patternWord.matcher(word.get(0));

            if (matcherword.find()) {
                sentence.set(i, sentence.get(i).replace(word.get(0),
                        words.stream().max(Comparator
                                .comparingInt(String::length)).orElse(null)));
            }

            logger.info(sentence.get(i));
        }
    }

    private void task6() {
        List<String> s = words.stream()
                .sorted()
                .collect(Collectors.toList());

        for (int i = 0; i < s.size(); i++) {

            if (i == s.size() - 1 || s.get(i).charAt(0)
                    == s.get(i + 1).charAt(0)) {
                System.out.print(s.get(i) + " ");
            } else {
                System.out.println(s.get(i) + " ");
            }
        }
    }

    private void task7() {
        Map<String, Double> mapCountOfVowels = new HashMap<>();

        for (String word : words) {
            int count = 0;

            for (int i = 0; i < word.length(); i++) {
                if ("AEIOUYaeiouy".indexOf(word.charAt(i)) > -1) {
                    count++;
                }
            }

            mapCountOfVowels.put(word, (double) count / (double) word.length());
        }

        mapCountOfVowels.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue())
                .forEach(System.out::println);
    }

    private void task8() {
        Map<String, String> mapFirstConsonant = new HashMap<>();
        ArrayList<String> wordStartsWithVowel = new ArrayList<>();
        String vowels = "aeiouAEIOU";

        for (String word : words) {
            if (vowels.indexOf(word.charAt(0)) > -1) {
                wordStartsWithVowel.add(word);
            }
        }

        for (String word : wordStartsWithVowel) {
            mapFirstConsonant.put(word, (consonant(word) == 1000)
                    ? "z" : String.valueOf(word.charAt(consonant(word))));
        }

        mapFirstConsonant.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(System.out::println);
    }

    private Map<String, Integer> forTask9AndTask13() {
        Map<String, Integer> mapOfSortedWords = new HashMap<>();
        logger.info("Enter character:");
        sc.nextLine();
        String ch = sc.nextLine();

        for (String word : words) {
            int count = 0;

            for (int i = 0; i < word.length(); i++) {
                if (ch.indexOf(word.charAt(i)) > -1) {
                    count++;
                }
            }

            mapOfSortedWords.put(word.toLowerCase(), count);
        }

        return mapOfSortedWords;
    }

    private void task9() {
        Map<String, Integer> mapOfSortedWords = forTask9AndTask13();

        mapOfSortedWords.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .sorted(Map.Entry.comparingByValue())
                .forEach(System.out::println);
    }

    private void task10() {
        Map<String, Integer> info = new HashMap<>();

        for (String word : words) {
            info.put(word, 0);
        }

        for (String sentence : sentences) {
            Map<String, Integer> info2 = new HashMap<>();

            for (String word : words) {
                info2.put(word, 0);
            }

            ArrayList<String> word = getWords(sentence);

            for (String str : word) {
                info2.put(str, info2.get(str) + 1);
                info.put(str, info.get(str) + 1);
            }

            logger.info(info2);
        }

        info.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .forEach(s -> logger.info(s));

    }

    private void task11() {
        logger.info("Enter first symbol");
        String symbol1 = sc.nextLine();
        logger.info("Enter second symbol");
        String symbol2 = sc.nextLine();

        for (String sentence : sentences) {
            logger.info(sentence);

            int minSymbol = sentence.length();
            int maxSymbol = -1;

            for (int i = 0; i < sentence.length(); i++) {

                if (String.valueOf(sentence.charAt(i))
                        .equals(symbol1) && i < minSymbol) {
                    minSymbol = i + 1;
                }

                if (String.valueOf(sentence.charAt(i))
                        .equals(symbol2) && i > maxSymbol) {
                    maxSymbol = i + 1;
                }
            }

            if (maxSymbol - minSymbol >= 0) {
                sentence = sentence.replaceAll(sentence
                        .substring(minSymbol, maxSymbol), "");
            }

            logger.info(sentence);
        }
    }

    private void task12() {
        logger.info("Enter length:");
        int length = sc.nextInt();

        logger.info(text);

        String consonants = "BCDFGHJKLMNPQRSTVWXZbcdfghjklmnpqrstvwxz";

        for (String word : words) {
            if (consonants.indexOf(word.charAt(0)) > -1 && word.length() == length) {
                text = text.replaceAll(word, " ");
            }
        }
        text = text.replaceAll("\\s+", " ");
        logger.info(text);
    }

    private void task13() {
        Map<String, Integer> mapOfSortedWords = forTask9AndTask13();
        mapOfSortedWords.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .forEach(System.out::println);
    }

    private void task14() {
        boolean[][] table = new boolean[text.length()][text.length()];
        int maxLength = 1;

        for (int i = 0; i < text.length(); i++) {
            table[i][i] = true;
        }

        int start = 0;

        for (int i = 0; i < text.length() - 1; i++) {
            if (text.charAt(i) == text.charAt(i + 1)) {
                table[i][i + 1] = true;
                start = i;
                maxLength = 2;
            }
        }

        for (int k = 3; k <= text.length(); k++) {
            for (int i = 0; i < text.length() - k + 1; i++) {
                int j = i + k - 1;
                if (table[i + 1][j - 1] && text.charAt(i) == text.charAt(j)) {

                    table[i][j] = true;

                    if (k > maxLength) {

                        start = i;
                        maxLength = k;
                    }
                }
            }
        }
        System.out.println("Palindrom start from: " + start
                + " and its length: " + maxLength);
    }

    private void task15() {
        logger.info("1.Last symbol");
        logger.info("2.First symbol");
        logger.info("Enter");
        int index = sc.nextInt();

        logger.info(text);

        if (index == 1) {

            for (String word : words) {
                String ch = String.valueOf(word.charAt(word.length() - 1));
                text = text.replaceAll(word, word
                        .replace(ch, "") + ch);
            }
        } else if (index == 2) {

            for (String word : words) {
                String ch = String.valueOf(word.charAt(0));
                text = text.replaceAll(word, ch + word
                        .replace(ch, ""));
            }
        } else {

            logger.error("Wrong index");
        }
        logger.info(text);
    }

    private void task16() {

        logger.info("Enter number of the sentence:");
        int index = sc.nextInt();
        logger.info("Enter length of the word to replace:");
        int length = sc.nextInt();
        logger.info("Enter string to replace:");
        sc.nextLine();
        String str = sc.nextLine();

        if (sentences.get(index - 1) != null) {

            logger.info("Before:");
            logger.info(sentences.get(index - 1));

            ArrayList<String> word = getWords(sentences.get(index - 1));

            boolean check = false;

            for (String s : word) {
                if (s.length() == length) {

                    sentences.set(index - 1, sentences.get(index - 1).replace(s, str));
                    check = true;
                }
            }

            if (!check) {

                logger.info("There is no words with length " + length);
            }

            logger.info("After");
            logger.info(sentences.get(index - 1));

        } else {

            logger.info("Wrong index");
        }
    }
}
