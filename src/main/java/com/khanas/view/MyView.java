package com.khanas.view;

import com.khanas.BigTask;
import com.khanas.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyView {

    private static Logger logger = LogManager.getLogger();
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> menuMethod;
    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("Q", bundle.getString("Q"));
    }

    public MyView() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        menuMethod = new LinkedHashMap<>();
        menuMethod.put("1", this::task1);
        menuMethod.put("2", this::task2);
        menuMethod.put("3", this::task3);
        menuMethod.put("4", this::task4);
        menuMethod.put("5", this::bigTask);
        menuMethod.put("6", this::internationalizeMenuEnglish);
        menuMethod.put("7", this::internationalizeMenuUkraine);
        menuMethod.put("8", this::internationalizeMenuItaliano);
        show();

    }

    private void bigTask() {
        new BigTask();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
    }

    private void internationalizeMenuItaliano() {
        locale = new Locale("it");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
    }


    private void internationalizeMenuUkraine() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
    }

    private void task1() {
        logger.info(new StringUtils().getString("Hi", null, 1, 2.3442,
                "name", null));
    }

    private void task2() {
        Pattern pattern = Pattern.compile("[A-Z][\\w',]*( [\\w',]+)*\\.");
        String text = "Lorem Ipsum is simply dummy text of the printing "
                + "and typesetting industry? Lorem Ipsum has been the industry's"
                + " standard dummy text ever since the 1500s, when an unknown"
                + " printer took a galley of type and scrambled it to make a type "
                + "specimen book.It has survived not only five centuries, but "
                + "also the leap into electronic typesetting, remaining essentially"
                + " unchanged.";
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            logger.info(text.substring(matcher.start(), matcher.end()));
        }
    }

    private void task3() {
        String text2 = "youarethebest";
        String pattern2 = "[t][h][e]|[y][o][u]";
        String[] arr = text2.split(pattern2);

        for (String str : arr) {
            if (!str.equals("")) {
                logger.info(str);
            }
        }
    }

    private void task4() {
        String text2 = "youarethebest";
        String pattern1 = "[AEIOUYaeiouy]";
        System.out.println(text2.replaceAll(pattern1, "_"));
    }

    private void outputMenu() {
        logger.info("MENU:");

        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    private void show() {
        String keyMenu;

        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();

            if (keyMenu.equals("Q")) {
                break;
            }

            menuMethod.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}



